var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3001;

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', socket => {
    //UI event handler
    socket.on('front', msg => {
        if (msg.constructor.name === "Object") {
            io.emit('front', JSON.stringify(msg));
        } else {
            io.emit('front', msg);
        }

    });

    //Python event handler
    socket.on('*', msg => {
        // setTimeout(()=>{
        //     socket.emit('response', `lol`/*{status: `success`}.toString()*/);
        // }, 10000)

        if (msg.constructor.name === "Object") {
            io.emit('response', `lol`/*{status: `success`}.toString()*/);
            io.emit('front', JSON.stringify(msg));
        } else {
            io.emit('response', `lol`/*{status: `success`}.toString()*/);
            io.emit('front', msg);
        }

    })
});

http.listen(port, function () {
    console.log('listening on *:' + port);
});
